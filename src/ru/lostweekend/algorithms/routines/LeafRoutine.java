package ru.lostweekend.algorithms.routines;

import ru.lostweekend.algorithms.DFS;
import ru.lostweekend.graph.Edge;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class LeafRoutine implements DFS.DFSRoutine<LinkedList<Integer>> {
    private LinkedList<Integer> mLeafs = new LinkedList<>();

    @Override
    public void run(int current, List<Edge> edges) throws InterruptedException, ExecutionException {
        if (edges.size() == 1) mLeafs.offer(current);
    }

    @Override
    public LinkedList<Integer> result() {
        return mLeafs;
    }
}
