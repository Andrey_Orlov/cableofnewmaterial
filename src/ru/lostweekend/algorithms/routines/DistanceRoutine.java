package ru.lostweekend.algorithms.routines;

import ru.lostweekend.algorithms.DFS;
import ru.lostweekend.graph.Edge;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class DistanceRoutine implements DFS.DFSRoutine<Integer> {
    private int mMaxDistance = 0;
    private Map<Integer, Integer> mVisited;

    public DistanceRoutine(int graphPartSize, int startPoint) {
        mVisited = new HashMap<>(graphPartSize);
        mVisited.put(startPoint, 0);
    }

    @Override
    public void run(int current, List<Edge> edges) throws InterruptedException, ExecutionException {
        for (Edge edge : edges) {
            if (!mVisited.containsKey(edge.getTarget())) {
                int targetTransmission = mVisited.get(current) + edge.getTransmission();
                mVisited.put(edge.getTarget(), targetTransmission);
                mMaxDistance = Integer.max(mMaxDistance, targetTransmission);
            }
        }
    }

    @Override
    public Integer result() {
        return mMaxDistance;
    }
}
