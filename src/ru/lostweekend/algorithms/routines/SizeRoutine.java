package ru.lostweekend.algorithms.routines;

import ru.lostweekend.algorithms.DFS;
import ru.lostweekend.graph.Edge;

import java.util.List;

public class SizeRoutine implements DFS.DFSRoutine<Integer> {
    private int mSize;

    @Override
    public void run(int current, List<Edge> edges) {
        mSize++;
    }

    @Override
    public Integer result() {
        return mSize;
    }
}
