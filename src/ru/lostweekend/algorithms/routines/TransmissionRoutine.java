package ru.lostweekend.algorithms.routines;

import ru.lostweekend.algorithms.DFS;
import ru.lostweekend.graph.Edge;
import ru.lostweekend.graph.Graph;

import java.util.*;
import java.util.concurrent.*;

public class TransmissionRoutine implements DFS.DFSRoutine<Integer> {
    private Integer mMaxTransmissionTime = 0;
    private Graph mGraph;
    private Set<RemovedEdge> mRemovedEdges = new HashSet<>();

    public TransmissionRoutine(Graph graph) {
        mGraph = graph;
    }

    @Override
    public void run(final int current, List<Edge> edges) throws InterruptedException, ExecutionException {
        if (edges.size() == 1) return;

        int procs = Runtime.getRuntime().availableProcessors();
        ExecutorService executorService = Executors.newFixedThreadPool(procs);

        Edge[] eds = Edge.copy(edges);
        for (Edge edge : eds) {
            final int target = edge.getTarget();
            RemovedEdge redge = new RemovedEdge(current, target);
            if (mRemovedEdges.add(redge)) {
                mGraph.removeEdge(current, target);

                LinkedList<Callable<Map<Class, Object>>> routines = new LinkedList<Callable<Map<Class, Object>>>() {{
                    offer(new DFS(current, mGraph, new SizeRoutine(), new LeafRoutine()));
                    offer(new DFS(target, mGraph, new SizeRoutine(), new LeafRoutine()));
                }};
                List<Future<Map<Class, Object>>> results = executorService.invokeAll(routines);
                assert results.size() == 2;

                routines = new LinkedList<>();
                for (int i = 0; i < 2; i++) {
                    Future<Map<Class, Object>> result = results.get(i);
                    Map<Class, Object> resultMap = result.get();
                    Integer treeSize = (Integer) resultMap.get(SizeRoutine.class);
                    LinkedList<Integer> leafs = (LinkedList<Integer>) resultMap.get(LeafRoutine.class);
                    for (int leaf : leafs) {
                        DFS dfs = new DFS(leaf, mGraph, new DistanceRoutine(treeSize, leaf)).setId(i);
                        routines.offer(dfs);
                    }
                }
                results = executorService.invokeAll(routines);

                int max1 = 0;
                int max2 = 0;
                for (Future<Map<Class, Object>> result : results) {
                    Map<Class, Object> resultMap = result.get();
                    Integer id = (Integer) resultMap.get(DFS.class);
                    Integer dist = (Integer) resultMap.get(DistanceRoutine.class);
                    if (id == 0) max1 = Integer.max(max1, dist);
                    else max2 = Integer.max(max2, dist);
                }
                int localMax = max1 + max2 + edge.getTransmission();
                mMaxTransmissionTime = Integer.max(mMaxTransmissionTime, localMax);

                mGraph.addEdge(edge.getTransmission(), current, target);
            }
        }
        executorService.shutdown();
    }

    @Override
    public Integer result() {
        return mMaxTransmissionTime;
    }

    private static final class RemovedEdge {
        private int mSource, mTarget;

        private RemovedEdge(int source, int target) {
            mSource = source;
            mTarget = target;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof RemovedEdge) {
                RemovedEdge other = (RemovedEdge) o;
                return this.hashCode() == other.hashCode();
            }
            return false;
        }

        @Override
        public int hashCode() {
            if (mSource > mTarget) return hashCode(mSource, mTarget);
            else return hashCode(mTarget, mSource);
        }

        private static int hashCode(int i1, int i2) {
            return 10000 * i1 + i2;
        }
    }
}
