package ru.lostweekend.algorithms;

import ru.lostweekend.graph.Edge;
import ru.lostweekend.graph.Graph;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

public class DFS implements Callable<Map<Class, Object>> {
    private final int mStartPoint;
    private final Graph mGraph;
    private final DFSRoutine[] mRoutines;
    private final Map<Class, Object> mResults;
    private Integer mId = null;

    public DFS(int startPoint, Graph graph, DFSRoutine... routines) {
        mStartPoint = startPoint;
        mGraph = graph;
        mRoutines = routines;
        mResults = new HashMap<>(routines.length);
    }

    public DFS setId(Integer id) {
        mId = id;
        return this;
    }

    @Override
    public Map<Class, Object> call() throws Exception {
        Set<Integer> used = new HashSet<>();
        Queue<Integer> visitQ = new LinkedList<>();
        for (Integer current = mStartPoint; current != null; current = visitQ.poll()) {
            if (used.add(current)) {
                List<Edge> edges = mGraph.getEdges(current);
                if (edges.size() > 0) {
                    for (Edge edge : edges) {
                        visitQ.offer(edge.getTarget());
                    }
                }
                for (DFSRoutine routine : mRoutines) routine.run(current, edges);
            }
        }
        mResults.put(DFS.class, mId);
        for (DFSRoutine routine : mRoutines) mResults.put(routine.getClass(), routine.result());
        return mResults;
    }

    public interface DFSRoutine<T> {
        void run(int current, List<Edge> edges) throws InterruptedException, ExecutionException;

        T result();
    }
}
