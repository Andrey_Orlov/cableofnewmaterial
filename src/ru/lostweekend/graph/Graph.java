package ru.lostweekend.graph;

import java.util.ArrayList;
import java.util.List;

public class Graph {
    private final int mVertexCount;
    private final List<List<Edge>> mVertexes;

    public Graph(int vertexCount) {
        mVertexCount = vertexCount;
        mVertexes = new ArrayList<>(vertexCount);
        for (int i = 0; i < mVertexCount; i++) mVertexes.add(new ArrayList<Edge>());
    }

    public void addEdge(int transmissionTime, int source, int target) {
        mVertexes.get(source).add(new Edge(transmissionTime, target));
        mVertexes.get(target).add(new Edge(transmissionTime, source));
    }

    public void removeEdge(int source, int target) {
        List<Edge> sourceEdges = mVertexes.get(source);
        removeEdge(sourceEdges, target);
        List<Edge> targetEdge = mVertexes.get(target);
        removeEdge(targetEdge, source);
    }

    private static void removeEdge(List<Edge> edges, int target) {
        for (int i = 0; i < edges.size(); i++) {
            if (edges.get(i).getTarget() == target) {
                edges.remove(i);
                break;
            }
        }
    }

    public List<Edge> getEdges(int vertex) {
        return mVertexes.get(vertex);
    }
}
