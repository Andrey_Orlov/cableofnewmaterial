package ru.lostweekend.graph;

import java.util.List;

public final class Edge {
    private final int mTransmission, mTarget;

    Edge(int transmission, Integer target) {
        mTransmission = transmission;
        mTarget = target;
    }

    private Edge(Edge other) {
        mTransmission = other.getTransmission();
        mTarget = other.getTarget();
    }

    public int getTransmission() {
        return mTransmission;
    }

    public int getTarget() {
        return mTarget;
    }

    public Edge cpy() {
        return new Edge(this);
    }

    public static Edge[] copy(List<Edge> edges) {
        Edge[] eds = new Edge[edges.size()];
        for (int i = 0; i < edges.size(); i++) eds[i] = edges.get(i).cpy();
        return eds;
    }
}
