package ru.lostweekend.input;

import ru.lostweekend.graph.Graph;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileInput {
    private int mGraphNumber = 0;
    private int mCaseCount;
    private BufferedReader mReader;

    public FileInput(String fileName) throws IOException {
        FileReader fileReader = new FileReader(fileName);
        mReader = new BufferedReader(fileReader);
        String sCaseCount = mReader.readLine();
        mCaseCount = Integer.valueOf(sCaseCount);
    }

    public int getGraphNumber() {
        return mGraphNumber;
    }

    public boolean isNotDone() {
        return mGraphNumber < mCaseCount;
    }

    public Graph nextGraph() throws IOException {
        String sVertexCount = mReader.readLine();
        int vertexCount = Integer.valueOf(sVertexCount);
        Graph graph = new Graph(vertexCount);
        String edgeLine;
        Pattern edgePattern = Pattern.compile("([0-9]+) ([0-9]+) ([0-9]+)");
        Matcher edgeMatcher;
        int shift = 1;
        for (int i = 0; i < vertexCount - 1; i++) {
            edgeLine = mReader.readLine();
            edgeMatcher = edgePattern.matcher(edgeLine);
            if (edgeMatcher.matches() && edgeMatcher.groupCount() == 3) {
                int transm = Integer.valueOf(edgeMatcher.group(1));
                int source = Integer.valueOf(edgeMatcher.group(2)) - shift;
                int target = Integer.valueOf(edgeMatcher.group(3)) - shift;
                graph.addEdge(transm, source, target);
            } else {
                throw new EdgeParseException(edgeLine);
            }
        }
        mGraphNumber++;
        return graph;
    }

    private static final class EdgeParseException extends IOException {
        private EdgeParseException(String sEdgeLine) {
            super(sEdgeLine);
        }
    }
}
