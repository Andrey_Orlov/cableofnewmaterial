package ru.lostweekend;

import ru.lostweekend.algorithms.DFS;
import ru.lostweekend.algorithms.routines.TransmissionRoutine;
import ru.lostweekend.graph.Graph;
import ru.lostweekend.input.FileInput;

import java.util.Map;

public class Main {
    public static void main(String[] args) throws Exception {
        FileInput input = new FileInput("sample_input.txt");
        while (input.isNotDone()) {
            Graph graph = input.nextGraph();
            DFS dfs = new DFS(0, graph, new TransmissionRoutine(graph));
            Map<Class, Object> results = dfs.call();
            Integer transmission = (Integer) results.get(TransmissionRoutine.class);
            System.out.printf("#%d %d%n", input.getGraphNumber(), transmission);
        }
    }
}
